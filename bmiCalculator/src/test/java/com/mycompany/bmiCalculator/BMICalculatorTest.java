package com.mycompany.bmiCalculator;

import static org.junit.Assert.assertEquals;

import java.security.InvalidParameterException;

import org.junit.Before;
import org.junit.Test;

public class BMICalculatorTest {
	private BMICalculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = new BMICalculator();
	}
	//pontosság teszt calculateBmi
	@Test
	public void testCalculateBMIGivesValidValue(){
		double result1 = calculator.calculateBmi(1.8, 60.0);
		double result2 = calculator.calculateBmi(1.7, 60.0);
		assertEquals(18.52, result1, 0.01);
		assertEquals(20.76, result2, 0.01);
	}
	//átváltás teszt convertToX
	@Test
	public void testCalculateBMIGivesValidValueAfterUnitConversion(){
		double result1 = calculator.calculateBmi(calculator.convertToM(1800, "mm"), 60.0);
		double result2 = calculator.calculateBmi(calculator.convertToM(17, "dm"), calculator.convertToKg(132.277357, "lbs"));
		assertEquals(18.52, result1, 0.01);
		assertEquals(20.76, result2, 0.01);
	}
	//besorolás teszt bmiInterpretaion
	@Test
	public void testbmiInterpretationGivesCorrectClassification(){
		String result1 = calculator.bmiInterpretation(16.5);
		String result2 = calculator.bmiInterpretation(50.0);
		assertEquals("Moderate Thinness", result1);
		assertEquals("Obese class III", result2);
	}
	//egy teljes számítási folyamat teszt
	@Test
	public void testbmiCalculationResultInterpretationGivesCorrectClassification(){
		double result = calculator.calculateBmi(calculator.convertToM(170, "cm"), 60.0); 
		String interpretation = calculator.bmiInterpretation(result);
		assertEquals("Normal", interpretation);
	}
	//hibás paraméter teszt calculateBmi
	@Test(expected = InvalidParameterException.class)
	public void testCalculateBMIWithInvalidParameters(){
		@SuppressWarnings("unused")
		double result = calculator.calculateBmi(-50, 1000);
	}
	//hibás paraméter teszt teljes folyamat
	@Test(expected = InvalidParameterException.class)
	public void testBmiCalculationProcessWithInvalidParameters(){
		@SuppressWarnings("unused")
		String result = calculator.bmiInterpretation(calculator.calculateBmi(calculator.convertToM(500, "dm"), 600));
	}
	//hibás paraméter teszt bmiInterpretaion
	@Test(expected = InvalidParameterException.class)
	public void testbmiCalculationResultInterpretationWithInvalidParameters(){
		@SuppressWarnings("unused")
		String result = calculator.bmiInterpretation(-300);
	}
}
