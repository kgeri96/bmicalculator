package com.mycompany.bmiCalculator;

public interface IBMICalculator {
	public double calculateBmi(double m_height, double kg_weight);
	public double convertToM(double height, String source);
	public double convertToKg(double weight, String source);
	public String bmiInterpretation(double bmi);
}
