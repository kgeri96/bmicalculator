package com.mycompany.bmiCalculator;

import java.security.InvalidParameterException;

/*
 * Ez az osztály a bmi kiszámítását végzi illetve a bmi alapján osztályba sorol és rendelkezik azon függvényekkel melyek elvégzik a mértékegységek közti konverziót ha az szükséges.
 * Az, hogy a bmi kiszámításához milyen mértékegységben állnak rendelkezésünkre adatok egy másik szinten dől el (pl user interface-en bekért érték + kiválasztható mértékegység).
 */

public class BMICalculator implements IBMICalculator{
	//BMI = weight(kg)/height^2(m^2)(Metric Units)
	public double calculateBmi(double m_height, double kg_weight) {
		if(m_height<=0 || m_height > 3){
			throw new InvalidParameterException("Invalid parameter exception!");
		}
		else if(kg_weight <=0 || kg_weight > 800){
			throw new InvalidParameterException("Invalid parameter exception!");
		}
		return kg_weight / (Math.pow(m_height, 2));
	}
	//BMI jelentése/osztályba sorolás
	public String bmiInterpretation(double bmi) {
		if(bmi <= 0 || bmi > 300){
			throw new InvalidParameterException("Implausible number! Check the input!");
		}
		else if(bmi < 16){
			return "Severe Thinness";
		}
		else if(bmi >= 16 && bmi < 17){
			return "Moderate Thinness";
		}
		else if(bmi >= 17 && bmi < 18.5){
			return "Mild Thinness";
		}
		else if(bmi >= 18.5 && bmi < 25){
			return "Normal";
		}
		else if(bmi >= 25 && bmi < 30){
			return "Overweight";
		}
		else if(bmi >= 30 && bmi < 35){
			return "Obese class I";
		}
		else if(bmi >= 35 && bmi < 40){
			return "Obese class II";
		}
		else if(bmi >= 40){
			return "Obese class III";
		}
		else{
			throw new InvalidParameterException("Invalid parameter exception!");
		}
	}
	//megadott magasság átváltása ha szükséges
	public double convertToM(double height, String source){
		if(source.equals("mm")){
			return height / 1000;
		}
		else if(source.equals("cm")){
			return height / 100;
		}
		else if(source.equals("dm")){
			return height / 10;
		}
		else if(source.equals("in")){
			return height / 39.37;
		}
		else if(source.equals("ft")){
			return height / 3.2808;
		}
		else{
			throw new InvalidParameterException("Invalid unit exception!");
		}
	}
	//megadott tömeg átváltása ha szükséges
	public double convertToKg(double weight, String source) {
		if(source.equals("g")){
			return weight / 1000;
		}
		else if(source.equals("dkg")){
			return weight / 100;
		}
		else if(source.equals("lbs")){
			return weight / 2.2046;
		}
		else if(weight <= 0 || weight > 800){
			throw new InvalidParameterException("Invalid parameter exception!");
		}
		else{
			throw new InvalidParameterException("Invalid unit exception!");
		}
	}

}
